// Defining age constants
const CURRENTAGE = 72;
const MAX_SCIENTISTS = 230;
const UI_VERSION = "0.0";


// main function call
run();

// defining the functions
// RUN
function run(){
	console.log('UtoIntel installed!');
	if(location.href.indexOf('/game/') != -1){	
		var prefs = getVal("settings", ["preferences"]);
		if( prefs !== null ){
			// console.log(prefs);
			if(prefs[0] == '1')
				modNav();
			else
				$("#munkbot_options > div:nth-child(1) > a").text("UtoIntel");
			if(prefs[1] == '1')
				collapsableHeaders();
			switch(gameLoc()){
				case 'preferences': preferences(); break;
				case 'throne': intel(); break;
				case 'send_armies': if(prefs[2] == '1') send_armies(); break;
			}
		}
		else{
			settings = {'preferences' 	: '1111111111',
						'uint_username' : '',
						'uint_password' : ''
						};
			setVal('settings',settings);
		}
	}
}

function intel(){
	console.log("Extracting Intel");
	if (location.href.indexOf('/wol/sit/')>-1) {
	    isSitting = '/sit';
	} else {
	    isSitting = '';
	}

	// Game header intel status icons
	var intelData = getVal("intelData");
	var ico = 'gray.png';
	var ts = Math.round((new Date()).getTime() / 3600000);
	console.log('Current Timestamp: ' + ts);	
	console.log('Inteldata stored is ' + intelData);

	// If undefined, set all data as old
	if( intelData === undefined){
		var intel = {'Throne'		: ts-1,
				 'State'		: ts-2,
				 'Military'		: ts-6,
				 'Buildings'	: ts-8,
				 'Science'		: ts-20,
				 'Mystics'		: ts-30,
				 'History'		: ts
				};
		setVal('intelData',intel);
	}
	
	// Handling Throne page separately bcoz of HTML structure
	var data = getVal('intelData',['Throne']);
	if(ts - data < 6) ico = 'green.png';
	else if(ts - data < 12) ico = 'orange.png';
	else if(ts - data < 24) { updateIntel('Throne'); ico = 'red.png'; }
	else updateIntel('Throne');

	$(".game-header > h1").html("Throne <img src='https://treasonguy.000webhostapp.com/experimental/" + ico + "' style='height:10px;width:10px;' title='" + (ts-data) + "hr'/>");
	
	// Handling other pages
	$(".game-header").children("a").each(function(i){
		if($(this).children("h1") !== undefined){
			let tab = $(this).children("h1").text();
			ico = 'gray.png';
			data = getVal('intelData',[tab]);
			console.log('Searching intel for tab: ' + tab);
			
			if(ts - data < 6) ico = 'green.png';
			else if(ts - data < 12) ico = 'orange.png';
			else if(ts - data < 24) { updateIntel(tab); ico = 'red.png'; }
			else updateIntel(tab);

		    tab += " <img src='https://treasonguy.000webhostapp.com/experimental/" + ico + "' style='height:10px;width:10px;' title='" + (ts-data) + "hr'/>";
			$(this).children("h1").html(tab);
		}
	});

	$(".game-header").append("<a><img onclick=updateAll(); src='https://treasonguy.000webhostapp.com/experimental/force.png' style='height:25px;width:25px;padding-top:2px;' title='Force Update' /></a>");
}

function updateAll(){
	let pages = ['Throne', 'State', 'Military', 'Buildings', 'Science', 'Mystics', 'History'];
	pages.forEach(function(element) {
		updateIntel(element);
	}, this);
	alert('Updated! Now the page will refresh.');
	location.reload();
}

function updateIntel(page){
	let url = '';
	switch(page){
		case 'Throne'		: url = 'http://utopia-game.com/wol/game/throne'; 			break;
		case 'State'		: url = 'http://utopia-game.com/wol/game/council_state'; 	break;
		case 'Military'		: url = 'http://utopia-game.com/wol/game/council_military'; break;
		case 'Buildings'	: url = 'http://utopia-game.com/wol/game/council_internal'; break;
		case 'Science'		: url = 'http://utopia-game.com/wol/game/science'; 			break;
		case 'Mystics'		: url = 'http://utopia-game.com/wol/game/council_spells'; 	break;
		case 'History'		: url = 'http://utopia-game.com/wol/game/council_history'; 	break;
	};

	// Updating intel update cookie
	let intelData = getVal('intelData');
	intelData[page] = Math.round((new Date()).getTime() / 3600000);
	setVal('intelData', intelData);
}

function send_armies(){
	console.log("Warroom");
	if (location.href.indexOf('/wol/sit/')>-1) {
	    isSitting = '/sit';
	} else {
	    isSitting = '';
	}

	// Adding autofill
	$("#send-armies-form > p").html("<input id='id_autofill_armies' type='submit' value='Autofill Military'>\
									<input id='id_send_armies' type='submit' value='Send Military'>");
	
	$(".two-column-stats").after("<h2>Military Strength</h2>\
								  <div id='milt' style='hidden:true;display:none;'> </div>\
								  <table style='padding:0px;'>\
									<tr><th id='OME_n'></th><td id='OME_v'></td>\
									<th id='DME_n'></th><td id='DME_v'></td>\
									</tr>\
									<tr><th id='Off_n'></th><td id='Off_v'></td>\
									<th id='Def_n'></th><td id='Def_v'></td>\
									</tr>\
								  </table>");
	$("#milt").load("http://utopia-game.com/wol"+isSitting+"/game/council_military .two-column-stats tbody tr:nth-child(1)", function(r){
		$("#OME_n").text('Offensive Military Effectiveness');
		$("#OME_v").text(($(r).find('.two-column-stats tbody tr:nth-child(1) td:nth-child(2)').text()));
		$("#DME_n").text(($(r).find('.two-column-stats tbody tr:nth-child(2) th:nth-child(1)').text()));
		$("#DME_v").text(($(r).find('.two-column-stats tbody tr:nth-child(2) td:nth-child(2)').text()));
		$("#Off_n").text(($(r).find('.two-column-stats tbody tr:nth-child(1) th:nth-child(3)').text()));
		$("#Off_v").text(($(r).find('.two-column-stats tbody tr:nth-child(1) td:nth-child(4)').text()));
		$("#Def_n").text(($(r).find('.two-column-stats tbody tr:nth-child(2) th:nth-child(3)').text()));
		$("#Def_v").text(($(r).find('.two-column-stats tbody tr:nth-child(2) td:nth-child(4)').text()));
	});
	$("#send-armies-form > table:nth-child(3) > tbody > tr:nth-last-child(1)").after("<tr><th>Def Home after attack<th><td id='def_left_home'>-</td>");
	
	$('#id_autofill_armies').bind('click', function(e) {
		e.preventDefault();			// prevents the form from being submitted
		console.log('Autofilling!');

		targetDef = $("#id_target_defense").val();
		if(targetDef === ''){
			$("#id_target_defense").val('DEF?');
			$("#id_target_defense").css('color','#e26f80');
		}
		else{
			targetDef = Math.ceil(parseInt(targetDef.replace(',','')) * 104 / parseFloat($("#OME_v").text().replace('%','')));
			TRGT = targetDef;

			totalOff = 0;
			iter = 0;
			stat = {};
			HORSE = new RegExp('war horses', 'gi');
			types = ["soldier", "ospec", "elite", "horse", "merc", "prisoner"];
			switch($("#send-armies-form > table:nth-child(3) > tbody > tr:nth-child(3) > th").text().toLowerCase()){
				case 'elf lords' : race = 'elf'; break;
				case 'drows' : race = 'dark elf'; break;
			}
			$("#send-armies-form > table:nth-child(3) > tbody > tr").each(function(){
				num = parseInt($(this).children("td").text().replace(',',''));
				if(isNaN(num)){
					num = 0;
				}
				if(iter != 3){
					stat[types[iter]] = num;
				}
				else{
					if(HORSE.test($(this).children("th").text())){
						stat[types[iter]] = num;
					}
					else{
						iter++;
						stat[types[iter]] = num;
					}
				}
				
				iter++;
			});
			
			totalOff = stat.soldier*RACEDETAILS[race].soldier.off + stat.ospec*RACEDETAILS[race].ospec.off + stat.elite*RACEDETAILS[race].elite.off;
			specs = stat.soldier + stat.ospec + stat.elite;
			stat.horses = Math.min(specs, stat.horses);
			stat.merc = Math.min((specs/5)-stat.prisoner, stat.merc);
			autoTotalPrisoner = 0;
			console.log(stat);

			// Calculating autofill values
			horsedOspec = Math.min(stat.ospec, stat.horse);
			autoHorsedOspec = Math.min(targetDef/(RACEDETAILS[race].ospec.off+RACEDETAILS[race].horse.off), horsedOspec);
			targetDef = Math.ceil(targetDef - (autoHorsedOspec*(RACEDETAILS[race].ospec.off+RACEDETAILS[race].horse.off)));
			stat.ospec = stat.ospec - autoHorsedOspec;
			
			autoPrisoner = Math.min(targetDef/RACEDETAILS[race].prisoner.off, autoHorsedOspec/5, stat.prisoner);
			targetDef = Math.ceil(targetDef - (autoPrisoner*RACEDETAILS[race].prisoner.off));
			stat.prisoner = stat.prisoner - autoPrisoner;
			autoTotalPrisoner = autoTotalPrisoner + autoPrisoner;
			
			autoOspec = Math.min(targetDef/RACEDETAILS[race].ospec.off, stat.ospec);
			targetDef = Math.ceil(targetDef - (autoOspec*RACEDETAILS[race].ospec.off));
			stat.ospec = stat.ospec - autoOspec;
			
			autoPrisoner = Math.min(targetDef/RACEDETAILS[race].prisoner.off, autoOspec/5, stat.prisoner);
			targetDef = Math.ceil(targetDef - (autoPrisoner*RACEDETAILS[race].prisoner.off));
			stat.prisoner = stat.prisoner - autoPrisoner;
			autoTotalPrisoner = autoTotalPrisoner + autoPrisoner;
			
			horsedElite = Math.min(stat.elite, stat.horse-horsedOspec);
			autoHorsedElite = Math.min(targetDef/(RACEDETAILS[race].elite.off+RACEDETAILS[race].horse.off), horsedElite);
			targetDef = Math.ceil(targetDef - (autoHorsedElite*(RACEDETAILS[race].elite.off+RACEDETAILS[race].horse.off)));
			stat.elite = stat.elite - autoHorsedElite;
			
			autoPrisoner = Math.min(targetDef/RACEDETAILS[race].prisoner.off, autoHorsedElite/5, stat.prisoner);
			targetDef = Math.ceil(targetDef - (autoPrisoner*RACEDETAILS[race].prisoner.off));
			stat.prisoner = stat.prisoner - autoPrisoner;
			autoTotalPrisoner = autoTotalPrisoner + autoPrisoner;
			
			autoElite = Math.min(targetDef/RACEDETAILS[race].elite.off, stat.elite);
			targetDef = Math.ceil(targetDef - (autoElite*RACEDETAILS[race].elite.off));
			stat.elite = stat.elite - autoElite;
			
			autoPrisoner = Math.min(targetDef/RACEDETAILS[race].prisoner.off, autoElite/5, stat.prisoner);
			targetDef = Math.ceil(targetDef - (autoPrisoner*RACEDETAILS[race].prisoner.off));
			stat.prisoner = stat.prisoner - autoPrisoner;
			autoTotalPrisoner = autoTotalPrisoner + autoPrisoner;
			
			horsedSoldier = Math.min(stat.soldier, stat.horse-horsedOspec);
			autoHorsedSoldier = Math.min(targetDef/(RACEDETAILS[race].soldier.off+RACEDETAILS[race].horse.off), horsedSoldier);
			targetDef = Math.ceil(targetDef - (autoHorsedSoldier*(RACEDETAILS[race].soldier.off+RACEDETAILS[race].horse.off)));
			stat.soldier = stat.soldier - autoHorsedSoldier;
			
			autoPrisoner = Math.min(targetDef/RACEDETAILS[race].prisoner.off, autoHorsedSoldier/5, stat.prisoner);
			targetDef = Math.ceil(targetDef - (autoPrisoner*RACEDETAILS[race].prisoner.off));
			stat.prisoner = stat.prisoner - autoPrisoner;
			autoTotalPrisoner = autoTotalPrisoner + autoPrisoner;
			
			autoSoldier = Math.min(targetDef/RACEDETAILS[race].soldier.off, stat.soldier);
			targetDef = Math.ceil(targetDef - (autoSoldier*RACEDETAILS[race].soldier.off));
			stat.soldier = stat.soldier - autoSoldier;
			
			autoPrisoner = Math.min(targetDef/RACEDETAILS[race].prisoner.off, autoSoldier/5, stat.prisoner);
			targetDef = Math.ceil(targetDef - (autoPrisoner*RACEDETAILS[race].prisoner.off));
			stat.prisoner = stat.prisoner - autoPrisoner;
			autoTotalPrisoner = autoTotalPrisoner + autoPrisoner;
			
			autoMerc = Math.min(targetDef/RACEDETAILS[race].mercs.off, stat.merc);
			targetDef = Math.ceil(targetDef - (autoMerc*RACEDETAILS[race].mercs.off));
			
			$("#id_infantry").val(Math.ceil(autoHorsedSoldier+autoSoldier));
			$("#id_offensive_specialist").val(Math.ceil(autoHorsedOspec+autoOspec));
			$("#id_elite").val(Math.ceil(autoHorsedElite+autoElite));
			$("#id_horse").val(Math.ceil(autoHorsedOspec + autoHorsedSoldier + autoHorsedElite));
			$("#id_mercenary").val(Math.ceil(autoMerc));
			$("#id_prisoner").val(Math.ceil(autoTotalPrisoner));
			
			$("#id_total_offense_points").text(Math.min(TRGT - targetDef, totalOff));
			$("#id_offensive_ratio").text(Math.round(parseFloat(TRGT - targetDef)/parseFloat(TRGT/104)), 2);

			// TODO : Soldier defense
			$("#def_left_home").text(parseInt($("#Def_v").text().replace(',','')) - (Math.ceil(RACEDETAILS[race].elite.off*$("#id_elite").val()*parseFloat($("#OME_v").text().replace('%',''))/100)));

			$("#send-armies-form > table:nth-child(3) > tbody > tr > td > input").css("color",'#FFF')
			if(targetDef >= 1){
				console.log('Unfulfilled!');
				$("#send-armies-form > table:nth-child(3) > tbody > tr > td > input").css("color",'#e26f80')
			}
		}
	});

	$("#send-armies-form > table:nth-child(3) > tbody > tr > td > input").focusout(function(){
		// TODO : Soldier defense
		$("#def_left_home").text(parseInt($("#Def_v").text().replace(',','')) - (Math.ceil(RACEDETAILS[race].elite.off*$("#id_elite").val()*parseFloat($("#OME_v").text().replace('%',''))/100)));
	});
}

function preferences(){
	console.log("Inside UTOINTEL settings");

	// Adding UTOINTEL settings
	$(".game-header").append("<a href='#utointel' id='utointel_prefs'><h1>UtoIntel Preferences</h1></a>");
	$("#utointel_prefs").on("click",function(){
		$(".game-header").html("<a href='/wol/game/preferences' >"+$("h1:contains(Game Preferences)")[0].outerHTML+"</a><h1>UtoIntel Preferences</h1>");
	
		// Adjusting the preferences
		$("div.game-content").html("<form><table style='margin-bottom: 5px' >\
										<tr>\
											<th>Username</th>\
											<td><input id='id_username' type='text' name='username' maxlength='30' style='width:220px;' /></td>\
											<th>Password</th>\
											<td><input type='password' name='password' id='id_password' style='width: 220px;' /></td>\
										</tr>\
									</table></form>\
									<table><tr>\
											<th id='opts' style='width'>Adjust Navigation</th>\
											<td><input id='nav' type='checkbox' type='text' style='background: #FFF;height:20px;width:17px;' );' /></td>\
											<th>Show Thronepage Sliders</th>\
											<td><input id='slider' type='checkbox' type='text' style='background: #FFF;height:20px;width:17px;' );' /></td>\
											<th>Autofill Military</th>\
											<td><input id='autoMilt' type='checkbox' type='text' style='background: #FFF;height:20px;width:17px;' );' /></td>\
										</tr>\
										<tr>\
											<th id='opts' style='width'>Reserved #1</th>\
											<td><input id='res1' type='checkbox' type='text' style='background: #FFF;height:20px;width:17px;' );' /></td>\
											<th>Reserved #2</th>\
											<td><input id='res2' type='checkbox' type='text' style='background: #FFF;height:20px;width:17px;' );' /></td>\
											<th>Reserved #3</th>\
											<td><input id='res3' type='checkbox' type='text' style='background: #FFF;height:20px;width:17px;' );' /></td>\
										</tr>\
									</table>\
									<form><input id='id_UT_prefs' onclick=UTprefs() type='submit' value='Save Settings' style='margin-top: 12px;' /></form>\
									");
		
		var username = getVal("settings", ["uint_username"]);
		var preferences = getVal("settings", ["preferences"]);

		if(preferences == '' || preferences == null){ preferences = '0000000000'; }
		
		$("#id_username").val(username);
		
		if(preferences[0] == 0){	$("#nav").prop('checked',false);	}
		else{	$("#nav").prop('checked',true);	}

		if(preferences[1] == '0'){	$("#slider").prop('checked',false);	}
		else{	$("#slider").prop('checked',true);	}

		if(preferences[2] == '0'){	$("#autoMilt").prop('checked',false);	}
		else{	$("#autoMilt").prop('checked',true);	}

		if(preferences[3] == '0'){	$("#res1").prop('checked',false);	}
		else{	$("#res1").prop('checked',true);	}

		if(preferences[4] == '0'){	$("#res2").prop('checked',false);	}
		else{	$("#res2").prop('checked',true);	}

		if(preferences[5] == '0'){	$("#res3").prop('checked',false);	}
		else{	$("#res3").prop('checked',true);	}
		
	});

}

function UTprefs(){
	event.preventDefault();
	console.log("Inside preferences click.");
	
	var username = document.getElementById("id_username").value;
	var password = document.getElementById("id_password").value;
	if($('#nav').is(":checked")){ var nav = '1'; }else{ var nav = '0'; }
	if($('#slider').is(":checked")){ var slider = '1'; }else{ var slider = '0'; }
	if($('#autoMilt').is(":checked")){ var autoMilt = '1'; }else{ var autoMilt = '0'; }
	if($('#res1').is(":checked")){ var res1 = '1'; }else{ var res1 = '0'; }
	if($('#res2').is(":checked")){ var res2 = '1'; }else{ var res2 = '0'; }
	if($('#res3').is(":checked")){ var res3 = '1'; }else{ var res3 = '0'; }
	
	$.post("https://treasonguy.000webhostapp.com/UtoIntel/loginUser.php",
	{
		username: username,
		password: password,
		action	: 'login'
	},
	function(data,status){
		if(status == 'success'){
			if(data === 'Success!'){
				preferences = nav+slider+autoMilt+res1+res2+res3;
				settings = {'preferences' 	: preferences,
							'uint_username' : username,
							'uint_password' : password
							}
				setVal('settings',settings);
				console.log('Saved!');
				alert("STATUS: " + data);
			}else{
				alert("STATUS: " + data);
			}
		}
	});
}

function modNav(){
	console.log('Beautifying navigation!');

	// Defining blocks
	var councilBlock = ["Throne","State","Armies","Buildings","Research","Spells","History"];
	var kingdomBlock = ["Kingdom","News","KD News"];
	var growthBlock = ["Explore","Growth","Raze","Cancel","Military","Rel Troops","Sciences","Wizards"];
	var aggressiveBlock = ["Mystics","Sorcery","Charms","Thievery","War Room","Rituals","Dragons","Aid"];
	var monarchBlock = ["Monarchy","Recruit","Invite", "Reservations"];
	var politicsBlock = ["Politics","War","Ceasefires","Relations","Targets","Rankings"];
	var forumBlock = ["Mail","Compose","Sent Items","Forum","War Forum","Preferences","Summary"];
	var settingsBlock = ["UtoIntel"," Auto: On"," Auto: Off",""];

	// Beautifying the navigation
	$("#middle > div:nth-child(3)").hide();
	$("#left-column > div:nth-child(1)").remove();
	$("#right-column").remove();
	//$("#google_center_div").remove();			//Google Ad bug
	$("#left-column").css("width","950px");
	$("#content-area").css("width","930px");
	$("#left-column > div:nth-child(1)").css("height","0px");
	$("#id_add_bookmark_form").remove();
	$("#id_remove_royal_command_form").remove();
	$("#id_set_royal_command_form").remove();

	// Accumulating current Navigation entries
	var currentNavValues = {};
	if ($("#munkbot_options > div:nth-child(1) > a").size() > 0) {
		$("#munkbot_options > div:nth-child(1) > a").text("UtoIntel");
	}

	$("#game-navigation").children(".navigation").each(function(i){
	    $(this).children("div").each(function(j){
	    	var onclick = $("a",this).attr("onclick");

	    	if (typeof onclick != 'undefined'){
	    		if(onclick.match(/disable_munkbot/g)) {
		    		currentNavValues['UtoIntel'] = $("a",this).attr("onclick");
		    	}
		    	else if (onclick.match(/auto_munkbot/g)) {
		    		currentNavValues[$("a",this).text()] = $("a",this).attr("onclick");
		    	}
		    	else if (onclick.match(/disable_custom_kdsite/g)) {
		    		currentNavValues['Custom Disable'] = $("a",this).attr("onclick");
		    	}
		    }
	    	else{
				text = $("a",this).text();
				text = text.replace(/\u00a0/g, " ");			// Replacing &nbsp;
				text = text.replace(/\s\u2666/g, "");			// Replacing &diams;
	    	    currentNavValues[text] = $("a",this).attr("href");
	    	}
	    });
	});
	// console.log(currentNavValues);

	if (location.href.indexOf('/wol/sit/')>-1) {
	    isSitting = 'sit/';
	} else {
	    isSitting = '';
	}

	var adv_m = $("#game-navigation > .navigation:nth-child(5)").find("span.advice-message").text();
	var adv_r = $("#game-navigation > .navigation:nth-child(6)").find("span.advice-message").text();

	currentNavValues["State"] 		= "/wol/"+isSitting+"game/council_state";
	currentNavValues["Armies"] 		= "/wol/"+isSitting+"game/council_military";
	currentNavValues["Buildings"] 	= "/wol/"+isSitting+"game/council_internal";
	currentNavValues["Research"] 	= "/wol/"+isSitting+"game/council_science";
	currentNavValues["Spells"] 		= "/wol/"+isSitting+"game/council_spells";
	currentNavValues["History"] 	= "/wol/"+isSitting+"game/council_history";
	currentNavValues["Raze"] 		= "/wol/"+isSitting+"game/raze";
	currentNavValues["Cancel"] 		= "/wol/"+isSitting+"game/cancel";
	currentNavValues["Rel Troops"] 	= "/wol/"+isSitting+"game/release_army";
	currentNavValues["Sorcery"] 	= "/wol/"+isSitting+"game/sorcery";
	currentNavValues["Charms"] 		= "/wol/"+isSitting+"game/charms";
	currentNavValues["Invite"] 		= "/wol/"+isSitting+"game/invitations";
	currentNavValues["Compose"] 	= "/wol/"+isSitting+"mail/compose/";
	currentNavValues["Sent Items"] 	= "/wol/"+isSitting+"mail/outbox/";
	currentNavValues["Relations"] 	= "/wol/"+isSitting+"game/conflict";
	currentNavValues["War"] 		= "/wol/"+isSitting+"game/war";
	currentNavValues["Ceasefires"] 	= "/wol/"+isSitting+"game/nap";
	currentNavValues["Summary"] = "http://home-world.org/utopia/formatter/?from=uto_ext' target='_blank";

	// Injecting the modified Nav
	// Left NAV
	var UINav = "";
	var blockstart = "<div class='navigation'>";
	var blockend = "</div>";

	UINav += blockstart;
	$.each(kingdomBlock, function(i){
		if (typeof currentNavValues[kingdomBlock[i]] != 'undefined')
			UINav += "<div><a href='"+currentNavValues[kingdomBlock[i]]+"'>"+kingdomBlock[i]+"</a></div>";
	});
	UINav += blockend;
	
	UINav += blockstart;
	$.each(councilBlock, function(i){
		if (typeof currentNavValues[councilBlock[i]] != 'undefined')
		UINav += "<div><a href='"+currentNavValues[councilBlock[i]]+"'>"+councilBlock[i]+"</a></div>";
	});
	UINav += blockend;

	UINav += blockstart;
	$.each(growthBlock, function(i){
		if (typeof currentNavValues[growthBlock[i]] != 'undefined')
		UINav += "<div><a href='"+currentNavValues[growthBlock[i]]+"'>"+growthBlock[i]+"</a></div>";
	});
	UINav += blockend;

	if (typeof currentNavValues['Monarchy'] != 'undefined') {
		UINav += blockstart;
		$.each(monarchBlock, function(i){
			if (typeof currentNavValues[monarchBlock[i]] != 'undefined'){
				if (monarchBlock[i] == "Mail") { 
					UINav += "<div><a href='"+currentNavValues[monarchBlock[i]]+"'>"+monarchBlock[i]+" <span class='advice-message'>"+adv_r+"</span></a></div>";
				}
				else{
					UINav += "<div><a href='"+currentNavValues[monarchBlock[i]]+"'>"+monarchBlock[i]+"</a></div>";
				}
			}
		});
		UINav += blockend;
	}

	$("#game-navigation > .navigation").remove();
	$("#game-navigation").append(UINav);

	// Right NAV
	var UINav = "<div id='game-navigation' style='float:right;padding-top: 0px;position:relative;z-index: auto;'>";
	var blockstart = "<div class='navigation' style='padding:6px 10px; width:100px; margin: 2px;'>";
	var blockend = "</div>";

	UINav += blockstart;
	$.each(aggressiveBlock, function(i){
		if (typeof currentNavValues[aggressiveBlock[i]] != 'undefined')
		UINav += "<div><a href='"+currentNavValues[aggressiveBlock[i]]+"'>"+aggressiveBlock[i]+"</a></div>";
	});
	UINav += blockend;
	
	UINav += blockstart;
	$.each(politicsBlock, function(i){
		if (typeof currentNavValues[politicsBlock[i]] != 'undefined'){
			if (politicsBlock[i] == "Targets") { 
				UINav += "<div><a href='http://treasonguy.tk/Utopia' target='_blank'>"+politicsBlock[i]+"</a> | <a href='"+currentNavValues[politicsBlock[i]]+"'>UT</a></div>";
			}
			else{
				UINav += "<div><a href='"+currentNavValues[politicsBlock[i]]+"'>"+politicsBlock[i]+"</a></div>";
			}
		}
	});
	UINav += blockend;

	UINav += blockstart;
	$.each(forumBlock, function(i){
		if (typeof currentNavValues[forumBlock[i]] != 'undefined'){
			if (forumBlock[i] == "Mail") { 
				UINav += "<div><a href='"+currentNavValues[forumBlock[i]]+"'>"+forumBlock[i]+" <span class='advice-message'>"+adv_m+"</span></a></div>";
			}
			else{
				UINav += "<div><a href='"+currentNavValues[forumBlock[i]]+"'>"+forumBlock[i]+"</a></div>";
			}
		}
	});
	UINav += blockend;

	UINav += blockstart;
	$.each(settingsBlock, function(i){
		if (typeof currentNavValues[settingsBlock[i]] != 'undefined')
		UINav += "<div><a href='#' onclick="+currentNavValues[settingsBlock[i]]+">"+settingsBlock[i]+"</a></div>";
	});
	UINav += blockend;


	UINav += '</div>';
	$("#content-area").prepend(UINav);
}

function collapsableHeaders(){
	console.log("Collapsing Headers!");
	var hidden = ["#throne-game-message > p",".throne-notice"];

	$.each(hidden, function(i){
		$(hidden[i]).hide(); 
	});

	$(".game-content").find("h2").each(function(i){
		$(this).click(function(){
	        $(this).next('p').slideToggle(600);
	    });
	});	
}

function gameLoc(){
	var URLDICT = {
		"game/kingdom_details"	: "kingdom_details",
		"game/kingdom_intel"	: "kingdom_intel",
		"game/province_news"	: "province_news",
		"game/kingdom_news"		: "kingdom_news",
		"game/throne"			: "throne",
		"game/council_state"	: "council_state",
		"game/council_military"	: "council_military",
		"game/council_internal"	: "council_internal",
		"game/council_science"	: "council_science",
		"game/council_spells"	: "council_spells",
		"game/council_history"	: "council_history",
		"game/explore"			: "explore",
		"game/build"			: "construct_build",
		"game/raze"				: "raze_build",
		"game/cancel"			: "cancel_build",
		"game/science"	: "science",
		"game/train_army"	: "train_army",
		"game/release_army"	: "release_army",
		"game/wizards"	: "wizards",
		"game/enchantment"	: "enchantment",
		"game/sorcery"	: "sorcery",
		"game/charms"	: "charms",
		"game/thievery"	: "thievery",
		"game/send_armies"	: "send_armies",
		"game/province_target_finder"	: "province_target_finder",
		"game/aid"	: "aid",
		"game/start_dragon"	: "start_dragon",
		"game/fund_dragon"	: "fund_dragon",
		"game/attack_dragon"	: "attack_dragon",
		"game/abandon_dragon"	: "abandon_dragon",
		"game/cast_ritual"	: "cast_ritual",
		"game/status_ritual"	: "status_ritual",
		"game/abandon_ritual"	: "abandon_ritual",
	};
	//console.log(location.href);
	loc = location.href;
	locstart = loc.indexOf('/game/');
	locend1 = (loc.indexOf("/",locstart+6) == -1) ? 100 : loc.indexOf("/",locstart+6);
	locend2 = (loc.indexOf("#",locstart+6) == -1) ? 100 : loc.indexOf("#",locstart+6);
	locend3 = (loc.indexOf("?",locstart+6) == -1) ? 100 : loc.indexOf("?",locstart+6);
	locend = Math.min(locend1,locend2,locend3);
	current_loc = (locstart > -1) ? location.href.substr(locstart+6, locend-locstart-6) : 'invalid';
	
	return current_loc;
}

function isSitting(){
	if (location.href.indexOf('/wol/sit/')>-1) {
	    isSitting = 1;
	} else {
	    isSitting = 0;
	}
	return isSitting;
}

function getVal(param, levels){
    if(localStorage.getItem(param) !== 'undefined'){
	    value = JSON.parse(localStorage.getItem(param));
    
    	if(levels !== 'undefined' && typeof levels !== 'undefined'){
    		levels.forEach(function(element) {
    			if(Object(value).hasOwnProperty(element)){
    				value = value[element];
    			}
    		}, this);
    	}
    }
	return value;
}

function setVal(param, value){
	if (typeof value == "object") {
		value["lastUpdated"] = Date.now();
		value = JSON.stringify(value);
	}
	try{
		localStorage.setItem(param, value);
	}catch(e){
		console.log(e);
	}finally{
		//console.log(value);
	}
}

var RACEDETAILS = {
	'human' : {
				 'soldier' : {
								'cost' : 0,
								'def' : 1,
								'name' : 'soldiers',
								'off' : 1,
								'nw' : 0
							  },
				 'prisoner' : {
								 'cost' : 0,
								 'def' : 0,
								 'name' : 'prisoners',
								 'off' : 5,
								 'nw' : 0
							   },
				 'dspecs' : {
							   'cost' : 0,
							   'def' : 6,
							   'name' : 'archers',
							   'off' : 0,
							   'nw' : 0
							 },
				 'mercs' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'mercenaries',
							  'off' : 5,
							  'nw' : 0
							},
				 'elites' : {
							   'cost' : 900,
							   'def' : 4,
							   'name' : 'knights',
							   'off' : 9,
							   'nw' : '10.0'
							 },
				 'thief' : {
							  'cost' : 500,
							  'def' : undef,
							  'name' : 'thieves',
							  'off' : undef,
							  'nw' : 0
							},
				 'horse' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'war horses',
							  'off' : 2,
							  'nw' : 0
							},
				 'ospecs' : {
							   'cost' : 0,
							   'def' : 0,
							   'name' : 'swordsmen',
							   'off' : 6,
							   'nw' : 0
							 }
			   },
	'avian' : {
				 'soldier' : {
								'cost' : 0,
								'def' : 1,
								'name' : 'soldiers',
								'off' : 1,
								'nw' : 0
							  },
				 'prisoner' : {
								 'cost' : 0,
								 'def' : 0,
								 'name' : 'prisoners',
								 'off' : 5,
								 'nw' : 0
							   },
				 'dspecs' : {
							   'cost' : 0,
							   'def' : 6,
							   'name' : 'harpies',
							   'off' : 0,
							   'nw' : 0
							 },
				 'mercs' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'mercenaries',
							  'off' : 5,
							  'nw' : 0
							},
				 'elites' : {
							   'cost' : 775,
							   'def' : 2,
							   'name' : 'drakes',
							   'off' : 9,
							   'nw' : '9.0'
							 },
				 'thief' : {
							  'cost' : 500,
							  'def' : undef,
							  'name' : 'thieves',
							  'off' : undef,
							  'nw' : 0
							},
				 'horse' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'war horses',
							  'off' : 2,
							  'nw' : 0
							},
				 'ospecs' : {
							   'cost' : 0,
							   'def' : 0,
							   'name' : 'griffins',
							   'off' : 6,
							   'nw' : 0
							 }
			   },
	'dryad' : {
				 'soldier' : {
								'cost' : 0,
								'def' : 1,
								'name' : 'soldiers',
								'off' : 1,
								'nw' : 0
							  },
				 'prisoner' : {
								 'cost' : 0,
								 'def' : 0,
								 'name' : 'prisoners',
								 'off' : 7,
								 'nw' : 0
							   },
				 'dspecs' : {
							   'cost' : 0,
							   'def' : 6,
							   'name' : 'nymphs',
							   'off' : 0,
							   'nw' : 0
							 },
				 'mercs' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'mercenaries',
							  'off' : 7,
							  'nw' : 0
							},
				 'elites' : {
							   'cost' : 900,
							   'def' : 2,
							   'name' : 'will o the wisps',
							   'off' : 12,
							   'nw' : '11.0'
							 },
				 'thief' : {
							  'cost' : 500,
							  'def' : undef,
							  'name' : 'thieves',
							  'off' : undef,
							  'nw' : 0
							},
				 'horse' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'war horses',
							  'off' : 4,
							  'nw' : 0
							},
				 'ospecs' : {
							   'cost' : 0,
							   'def' : 0,
							   'name' : 'huldras',
							   'off' : 6,
							   'nw' : 0
							 }
			   },
	'faery' : {
				 'soldier' : {
								'cost' : 0,
								'def' : 1,
								'name' : 'soldiers',
								'off' : 1,
								'nw' : 0
							  },
				 'prisoner' : {
								 'cost' : 0,
								 'def' : 0,
								 'name' : 'prisoners',
								 'off' : 5,
								 'nw' : 0
							   },
				 'dspecs' : {
							   'cost' : 0,
							   'def' : 6,
							   'name' : 'druids',
							   'off' : 0,
							   'nw' : 0
							 },
				 'mercs' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'mercenaries',
							  'off' : 5,
							  'nw' : 0
							},
				 'elites' : {
							   'cost' : 900,
							   'def' : 9,
							   'name' : 'beastmasters',
							   'off' : 4,
							   'nw' : '10.5'
							 },
				 'thief' : {
							  'cost' : 500,
							  'def' : undef,
							  'name' : 'thieves',
							  'off' : undef,
							  'nw' : 0
							},
				 'horse' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'war horses',
							  'off' : 2,
							  'nw' : 0
							},
				 'ospecs' : {
							   'cost' : 0,
							   'def' : 0,
							   'name' : 'magicians',
							   'off' : 6,
							   'nw' : 0
							 }
			   },
	'orc' : {
			   'soldier' : {
							  'cost' : 0,
							  'def' : 1,
							  'name' : 'soldiers',
							  'off' : 1,
							  'nw' : 0
							},
			   'prisoner' : {
							   'cost' : 0,
							   'def' : 0,
							   'name' : 'prisoners',
							   'off' : 5,
							   'nw' : 0
							 },
			   'dspecs' : {
							 'cost' : 0,
							 'def' : 6,
							 'name' : 'trolls',
							 'off' : 0,
							 'nw' : 0
						   },
			   'mercs' : {
							'cost' : 0,
							'def' : 0,
							'name' : 'mercenaries',
							'off' : 5,
							'nw' : 0
						  },
			   'elites' : {
							 'cost' : 900,
							 'def' : 1,
							 'name' : 'ogres',
							 'off' : 10,
							 'nw' : '9.5'
						   },
			   'thief' : {
							'cost' : 500,
							'def' : undef,
							'name' : 'thieves',
							'off' : undef,
							'nw' : 0
						  },
			   'horse' : {
							'cost' : 0,
							'def' : 0,
							'name' : 'war horses',
							'off' : 2,
							'nw' : 0
						  },
			   'ospecs' : {
							 'cost' : 0,
							 'def' : 0,
							 'name' : 'goblins',
							 'off' : 6,
							 'nw' : 0
						   }
			 },
	'bocan' : {
				 'soldier' : {
								'cost' : 0,
								'def' : 1,
								'name' : 'soldiers',
								'off' : 1,
								'nw' : 0
							  },
				 'prisoner' : {
								 'cost' : 0,
								 'def' : 0,
								 'name' : 'prisoners',
								 'off' : 5,
								 'nw' : 0
							   },
				 'dspecs' : {
							   'cost' : 0,
							   'def' : 6,
							   'name' : 'imps',
							   'off' : 0,
							   'nw' : 0
							 },
				 'mercs' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'mercenaries',
							  'off' : 5,
							  'nw' : 0
							},
				 'elites' : {
							   'cost' : 500,
							   'def' : 7,
							   'name' : 'tricksters',
							   'off' : 4,
							   'nw' : '7.0'
							 },
				 'thief' : {
							  'cost' : 500,
							  'def' : undef,
							  'name' : 'thieves',
							  'off' : undef,
							  'nw' : 0
							},
				 'horse' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'war horses',
							  'off' : 2,
							  'nw' : 0
							},
				 'ospecs' : {
							   'cost' : 0,
							   'def' : 0,
							   'name' : 'marauders',
							   'off' : 6,
							   'nw' : 0
							 }
			   },
	'elf' : {
			   'soldier' : {
							  'cost' : 0,
							  'def' : 1,
							  'name' : 'soldiers',
							  'off' : 1,
							  'nw' : 0
							},
			   'prisoner' : {
							   'cost' : 0,
							   'def' : 0,
							   'name' : 'prisoners',
							   'off' : 5,
							   'nw' : 0
							 },
			   'dspecs' : {
							 'cost' : 0,
							 'def' : 7,
							 'name' : 'archers',
							 'off' : 0,
							 'nw' : 0
						   },
			   'mercs' : {
							'cost' : 0,
							'def' : 0,
							'name' : 'mercenaries',
							'off' : 5,
							'nw' : 0
						  },
			   'elites' : {
							 'cost' : 700,
							 'def' : 6,
							 'name' : 'elf lords',
							 'off' : 7,
							 'nw' : '8.5'
						   },
			   'thief' : {
							'cost' : 500,
							'def' : undef,
							'name' : 'thieves',
							'off' : undef,
							'nw' : 0
						  },
			   'horse' : {
							'cost' : 0,
							'def' : 0,
							'name' : 'war horses',
							'off' : 2,
							'nw' : 0
						  },
			   'ospecs' : {
							 'cost' : 0,
							 'def' : 0,
							 'name' : 'rangers',
							 'off' : 6,
							 'nw' : 0
						   }
			 },
	'dwarf' : {
				 'soldier' : {
								'cost' : 0,
								'def' : 1,
								'name' : 'soldiers',
								'off' : 1,
								'nw' : 0
							  },
				 'prisoner' : {
								 'cost' : 0,
								 'def' : 0,
								 'name' : 'prisoners',
								 'off' : 5,
								 'nw' : 0
							   },
				 'dspecs' : {
							   'cost' : 0,
							   'def' : 6,
							   'name' : 'axemen',
							   'off' : 0,
							   'nw' : 0
							 },
				 'mercs' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'mercenaries',
							  'off' : 5,
							  'nw' : 0
							},
				 'elites' : {
							   'cost' : 900,
							   'def' : 5,
							   'name' : 'berserkers',
							   'off' : 8,
							   'nw' : '10.0'
							 },
				 'thief' : {
							  'cost' : 500,
							  'def' : undef,
							  'name' : 'thieves',
							  'off' : undef,
							  'nw' : 0
							},
				 'horse' : {
							  'cost' : 0,
							  'def' : 0,
							  'name' : 'war horses',
							  'off' : 2,
							  'nw' : 0
							},
				 'ospecs' : {
							   'cost' : 0,
							   'def' : 0,
							   'name' : 'warriors',
							   'off' : 6,
							   'nw' : 0
							 }
			   },
	'dark elf' : {
					'soldier' : {
								   'cost' : 0,
								   'def' : 1,
								   'name' : 'soldiers',
								   'off' : 1,
								   'nw' : 0
								 },
					'prisoner' : {
									'cost' : 0,
									'def' : 0,
									'name' : 'prisoners',
									'off' : 5,
									'nw' : 0
								  },
					'dspecs' : {
								  'cost' : 0,
								  'def' : 6,
								  'name' : 'druids',
								  'off' : 0,
								  'nw' : 0
								},
					'mercs' : {
								 'cost' : 0,
								 'def' : 0,
								 'name' : 'mercenaries',
								 'off' : 5,
								 'nw' : 0
							   },
					'elites' : {
								  'cost' : 700,
								  'def' : 7,
								  'name' : 'drows',
								  'off' : 5,
								  'nw' : '9.0'
								},
					'thief' : {
								 'cost' : 500,
								 'def' : undef,
								 'name' : 'thieves',
								 'off' : undef,
								 'nw' : 0
							   },
					'horse' : {
								 'cost' : 0,
								 'def' : 0,
								 'name' : 'war horses',
								 'off' : 2,
								 'nw' : 0
							   },
					'ospecs' : {
								  'cost' : 0,
								  'def' : 0,
								  'name' : 'night rangers',
								  'off' : 8,
								  'nw' : 0
								}
				  }
  };
